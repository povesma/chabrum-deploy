#!/bin/sh
sudo touch ENV_PARAMS
RES=$(cat ENV_PARAMS | grep __INSTANCE__)
if [ -z "$?" ]; then
	read -p  "It seems that Chabrum environment has already been configured. Configure anyway? [y | n]: " ANYWAY
	if [ "$ANYWAY" != "y" && "$ANYWAY" != "Y" && "$ANYWAY" != "Yes" && "$ANYWAY" != "yes" ]; then
		echo "Exitting Chabrum config."
		exit 1
	fi
fi

# CHAPTER 1 - Install software - docker, nginx, tools...
	# for mongo

sudo apt update
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
sudo apt-get install gnupg gnupg2 pass
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
sudo apt update

#sudo apt install -y mongodb-org-tools
# mongodb-clients mongo-tools
sudo apt install -y software-properties-common ca-certificates apt-transport-https
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt install -y nginx docker-ce docker-compose apache2-utils
docker plugin install  grafana/loki-docker-driver:latest --alias loki --grant-all-permissions

export MONGO_INSTALLER_FN=mongodb-org-tools_4.2.1_amd64.deb
wget https://repo.mongodb.org/apt/ubuntu/dists/bionic/mongodb-org/4.2/multiverse/binary-amd64/$MONGO_INSTALLER_FN -P /tmp && sudo dpkg -i /tmp/$MONGO_INSTALLER_FN
rm /tmp/$MONGO_INSTALLER_FN

sudo apt install -y letsencrypt python-certbot-nginx
sudo apt autoremove  -y

# CHAPTER 2 - Directory structure: nginix_logs, purple...
#( or copy from pre-set archive)

sudo mkdir -p nginx_logs
sudo mkdir -p purple_files/files
sudo mkdir -p purple_files/videos
sudo mkdir -p purple_files/photos
sudo mkdir -p purple_files/voice

sudo mkdir -p grafana
sudo mkdir -p grafana/etc
sudo mkdir -p grafana/log
sudo mkdir -p grafana/storage

mkdir -p prometheus
mkdir -p prometheus/data

sudo chown -R 1000:docker prometheus

read -p "Domain name (FQDN)? " FQDN
read -p "Instance (case sensitive)? " INSTANCE

echo "--> Find your Telegram User ID by sending /me to @logmein_bot on the Telegram"
read "Telegram Admin numeric UserID: " TGADMIN

sudo touch ENV_PARAMS
sudo touch docker-compose.yml

sudo chown $USER ENV_PARAMS
sudo chown $USER docker-compose.yml

sudo touch top_level.conf
sudo chown $USER top_level.conf

sudo touch grafana/etc/grafana.ini
sudo chown -R 472:docker grafana

sed "s/__DOMAIN__/$FQDN/g; s/__INSTANCE__/$INSTANCE/g; s/__TGADMIN__/$TGADMIN/g" ENV_PARAMS.template > ENV_PARAMS
sed "s/__DOMAIN__/$FQDN/g; s/__INSTANCE__/$INSTANCE/g; s/__TGADMIN__/$TGADMIN/g" docker-compose.yml.template > docker-compose.yml
sed "s/__DOMAIN__/$FQDN/g" top_level.conf.template > top_level.conf
sed "s/__DOMAIN__/$FQDN/g" grafana/etc/grafana.ini.template > grafana/etc/grafana.ini

echo
echo "Lets create a password to protect Chabrum instance and allow you access from web"
sudo htpasswd .htpasswd-chabrum admin

echo
echo "Authorization for private Docker registry (registry1.chabrum.com) (consider user 'jenkins')"
docker login registry1.chabrum.com
LOGINRESULT=$?
if [ "$LOGINRESULT" = "0" ]; then
	echo "Docker registry auth successful. You can start Chabrum by typing 'docker-compose up -d'"
else
	echo "Wrong password!"
	exit 1
fi

# CHAPTER 3 - Config files: docker-compose.yaml, /etc/nginx/nginx.conf, htpasswd
sudo cp top_level.conf /etc/nginx/sites-enabled/chabrum_in_docker.conf
sudo cp upstream.cnf /etc/nginx/sites-enabled
sudo chown -R root /etc/nginx/sites-enabled
sudo sed -i s/"include\ \/etc\/nginx\/sites-enabled\/\*\;"/"include\ \/etc\/nginx\/sites-enabled\/\*.conf\;"/ /etc/nginx/nginx.conf
sudo service nginx reload

# CHAPTER 4 - Specific configuration: letsencrypt, jenkins user, access control
# sudo deluser jenkins && sudo rm -rf /home/jenkins
sudo adduser --disabled-password -q --gecos "" jenkins
sudo -u jenkins mkdir -p /home/jenkins/.ssh
sudo -u jenkins touch /home/jenkins/.ssh/authorized_keys
cat << EOF | sudo -u jenkins tee /home/jenkins/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDAj4pnH9zC4+qLswjdTE63ur9GdJApeUXWyqqykCNqpPIPZHlU7YemdT2uDl4onhMxaRFYtippTHHNfDOdld7oPFcO1+zm+TBccz4vWWZS4YQIiASI3gvIXhIQ9hzp6YbqgXnKFfVTpxZqGGrDQ862yfEXOKWY+nEmhKqEyZkKWIEU2oO54c6NyzCXTm97qHyR/5QAAiJL13uda40lHokWoQB1EWYlJV++bMV29OWC/mlPN6EANqDXj+wS5qcaJUizXWSXwqaqZxPUupVvD7WyeFNWCD2417VI1amYSqCLoQFCleK6CFCwaKsMCpd9NpPe/yByRkrv6uot4EXm4WCp jenkins@dev.cloudi.es
EOF

sudo usermod -aG docker jenkins

sudo letsencrypt
