# What is Chabrum

Chabrum is an infrastucture to develop, run and support chatbots (primarily) and other API-based software.

It consist of:

- Chabrum-player: the player or the chatbot logic
- Puprple: handles communication with chat networks (Telegram, Facebook, Viber)
- Endpointer: provides API / web endpoints, served by the bot code
- Megatimer: schedules future and immediate tasks
- Node-VM: safely runs node-js code
- Nginx web-server that routes requests (using reverse-proxy)

Additional components used for observability and monitoring:
- Prometheus
- Grafana
- Loki

The chatbot code (logic) is loaded after the system deployment.

# Pre-requisites

Chabrum is an Internet-software, and it requires a Linux (Ubuntu 16.04+) server (2Gb+ RAM, 50Gb+ HDD) with Internet connection. 
Besides that, you shall have the following:

- your Internet domain name
- Web-server software, that will handle all incoming connections and provide security (nginx)
- ssl-configuration for web-server (letsencrypt)
- docker & docker-composer to run the installation

All items are must to have.

# Infrastriucture deploy
```
sudo apt update
sudo apt install -y mongodb-org-tools
sudo apt install -y docker docker.io docker-compose apache2-utils mongodb-clients mongo-tools gnupg2 pass 
sudo apt install -y letsencrypt python-certbot-nginx
```

# How to install Chabrum

0. clone this repository: `git clone https://povesma@bitbucket.org/povesma/chabrum-deploy.git && cd chabrum-deploy`
0. Run `setup.sh` and type in your domain name, Telegram ID of the system admin (numeric) and you `instance name`
0. Set required ENV variables in docker-compose.yml (see sections purple and chabrum-player, mainly you need to replace 
YOUR_DOMAIN with domain name or IP-address
0. `docker login registry1.chabrum.com` and type in acquired login / password
0. `docker-compose up -d` (omit `-d` to see full output, or `docker-compose logs -f --tail 100`)

Done. System is up and running in 7 docker containers. But you still can't use it as a chatbot: 
you need to route external connections to it.

# Manage Chabrum

0. Stop everything: `docker-compose down`
0. Check how it's going: `docker ps`
0. Restart something: `docker-compose stop _SERVICE_NAME_; docker-compose up -d`

# Configure host nginx

Add the following lines to the `server` section of Nginx configuration for you domain name:
```
location ~ .* {
  proxy_pass http://127.0.0.1:8080;
  proxy_set_header Host $http_host;
  proxy_set_header X-Real-IP $remote_addr;
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header X-Forwarded-Proto $scheme;
  client_max_body_size 100m;
}
```

This will forward all external traffic to internal Chabrum web-server (nginx), which will do all the rest.

# Protected access to Chabrum Editor

## Basic Authentication - web server level
Chabrum Editor enables bot editing, access to logs and much more, so it should be properly configured for the security.
Default login and password are specified in `.htpasswd-chabrum` file (`admin/laZagn1a909`). 
**Update this file** with your own credntials and restart the `nginx` docker container (or entire system, no worries).

`htpasswd .htpasswd-chabrum admin`

Changes will come in force immediately, no restart needed.

## 4digit auth - application level (2nd factor)

TBD

# Upload chatbot logic

It comes in 4 files: obtain them from your developer. Go to `https://you_domain/fsm/panel` and upload files one by one.

Alternatively, grant access to chabrum editor for your developer: they know what to do.

Files with logic wil look like:

`fsm___2019-07-18_13-21.json			Bot code itself`

`hosts_2019-07-18_13-22.json			Hosts description`

`shortcuts_2019-07-18_13-22.json		Endpoints / path description`

`namespaces_2019-07-18_13-22.json		Namespaces comments and metadata, endpoints information`


Go to `https://your_domain/fsm/panel` and:

* Create a database - give it a reasonable name with Latin letters, numbers, _, -
* Click on File "button" in the "Load" column
* Load files one by one, confirming all questions
* Tick "Editable?" on 

In `When to use? (JSON condition)` column, top field:

`[{"data":{"user":{"tags":["TAG"]}}}]` (replace TAG) with one use used when adding a Messenger account

## Configuring Messengers

### Telegram

1. Create a bot and send any message to your bot.
1. At the bottom of the page insert your bot's token and type in the TAG, which is used in the `When to use? (JSON condition)` 
1. You will receive a confirmation message to the bot.

### Viber
Go to `https://your_domain/fsm/panel` and:
1. At the bottom of the page insert your bot's token and type in the TAG, which is used in the `When to use? (JSON condition)` 

#### Manually
Mongo DB, database `purple`, insert into collection `bot_settings`:

`{"bot_type" : "viber",  "tags" : [ "TAG" ], "botname" : "TECHNICAL_BOT_NAME, "name" : "Name of bot / page", "token" : "VIBER_KEY" }`


`name` should be <=32 characters, latin letters, numbers, -, _

`botname` should be <=32 characters, latin letters, numbers, -, _ (make the same as name)

`token` - your page token, obtained via FB Application


### Facebook Messenger

#### Manually
Mongo DB, database `purple`, insert into collection `bot_settings`:

`{"bot_type" : "fbbot", "pageid" : "YOUR_NUMERIC_PAGE_ID", "tags" : [ "TAG" ], "name" : "Name of bot / page", "token" : "PAGE_TOKEN" }`


`name` should be <=32 characters, latin letters, numbers, -, _

`botname` should be <=32 characters, latin letters, numbers, -, _ (make the same as name)

`token` - your page token, obtained via FB Application

`pageid` - numeric page_id (plenty of services on the Internet help to obtain it)

## Add JS Editor plugin

Go to `https://you_domain/fsm/panel`, bottom of the page, add jseditor.html from this repo and give it a decent name (JS-EDITOR)

Go to `https://you_domain/fsm/`, select your database, go to Shortcuts, find `js` shortcut, select `JS-EDITOR` 

## Config

### Variables / keys / passwords / etc

Go to `https://you_domain/fsm/`, select your database, select `~config` in the list of namespaces, click 'Debug | Edit action'. 
This is a JS that defines config variables values. Fix them according to what you need.

### Google Maps API

Go to `https://you_domain/fsm/`, select your database, go to Shortcuts, find `MapAPI` shortcut, in PATH replace 
everything after `&key=` with your own Google Maps API key (Geocoding).

## Translations / Dictionary

Upload the dictionary from MongoDB backup files (unpack them first if archived):

`mongorestore --db YOU_DB_NAME PATH_TO_BACKUP_FILES`

## Creating engpoints

If there any endpoints in the bot, you need to recreate them. Check `namespaces.json` to learn if there any endpoints - look for `"endpoint"`.
Note the names of namespaces, remove the lines with `"endpoint"` and load `namespaces.json` again.

Select the database, and then select the namespace from the list with enadpoint. Press the green button `Set Endpoint`. Endpoint address 
will appear instead of the button.